//define socket
const socket = require("ws");

//define users
const Users = require("./users");

//define debug
const Debug = require("../debug/debug");

//define UUID
const UUID = require("uuid/v4");

//define Map
const Map = require("./map");

//define socket server
const wsServer = new socket.Server(
    {
        port: 1337
    }
);

/**
 * Connection function to be called on any new connection stabilished.
 * @param {WebSocket} webSocket Incoming websocket.
 */
function Connection(webSocket)
{
    Debug.LogAction("User connected to the user.");

    //generate new Unique ID
    let uuid = UUID();

    //setup message for the user
    var setupMessage =
    {
        type: "config",
        ID: uuid,
        Obstacles: Map.Obstacles
    }

    //Send setup message to new user
    webSocket.send(JSON.stringify(setupMessage));

    //Add user to pool
    Users.AddUser(webSocket, uuid);

    //setup user websocket onMessage event
    webSocket.on("message", function (message)
    {
        Message(message, webSocket);
    });

    //user disconnected
    webSocket.on("close", function (close)
    {
        Close(close, webSocket, uuid);
    });
}

/**
 * Called every time a message is received from an user.
 * @param {String|Buffer|ArrayBuffer|Buffer[]} message 
 */
function Message(message, webSocket)
{
    var messageData = JSON.parse(message);

    Debug.LogAction(["Message received: ", message]);

    //undefined type messages are player updates
    if (messageData.type == undefined)
    {
        //broadcast Update
        for (var key in Users.ConnectedUsers)
        {
            if (Users.ConnectedUsers[key].WebSocket != webSocket)
            {
                Users.ConnectedUsers[key].WebSocket.send(message);
            }
        }

        //stop function
        return;
    }

    //Player is now ready to go display on other players'screen. 
    if (messageData.type == "initPlayer")
    {
        var initedUser = Users.ConnectedUsers[messageData.localPlayer.id];

        //Player is Initialized on server
        initedUser.Ready = true;
        initedUser.Color = messageData.localPlayer.color;

        //Init data from the new player
        let initMessage = { type: "initPlayer", ID: initedUser.ID, Color: initedUser.Color };

        //Each already connected user receives data from this new user
        for (var key in Users.ConnectedUsers)
        {
            Users.ConnectedUsers[key].WebSocket.send(JSON.stringify(initMessage));
        }


        //The new user must receives data from all other connected users
        for (var key in Users.ConnectedUsers)
        {
            if (key != initedUser.ID)
            {
                let otherUser = { type: "initPlayer", ID: key, Color: Users.ConnectedUsers[key].Color };
                initedUser.WebSocket.send(JSON.stringify(otherUser));
            }
        }

        //stop function
        return;
    }
}

/**
 * Called every time an user disconnects from the server.
 * @param {int} close 
 * @param {WebSocket} webSocket 
 */
function Close(close, webSocket, uuid)
{
    Debug.LogError([webSocket, " close. Code: ", close]);
    Users.RemoveUser(webSocket);

    let disconnectedMessage =
    {
        type: "disconnected",
        id: uuid
    }

    //Each yet connected users receives data from disconnected one
    for (var key in Users.ConnectedUsers)
    {
        Users.ConnectedUsers[key].WebSocket.send(JSON.stringify(disconnectedMessage));
    }
}

/**
 * Starts the web socket server.
 */
function Start()
{
    wsServer.on("connection", Connection);
}


module.exports.Start = Start;