
var Obstacles = [];

for (i = 0; i <= 100; i++)
{
    let obstacle = {};

    let scale = {};
    scale.x = (Math.random() * 0.5) + 0.5;
    scale.y = (Math.random() * 0.5) + 0.5;
    scale.z = (Math.random() * 0.5) + 0.5;

    let position = {};
    position.x = Math.random() * 50 - 25;
    position.y = scale.y * 0.5;
    position.z = Math.random() * 50 - 25;

    obstacle.position = position;
    obstacle.scale = scale;

    Obstacles.push(obstacle);
}

module.exports.Obstacles = Obstacles;