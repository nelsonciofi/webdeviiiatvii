//define User
const User = require("./user").User;

//define Debug
const Debug = require("../debug/debug");


/**
 * A list of current connected users/players.
 */
var ConnectedUsers = {};

//The number of users connected to the server.
var UserCount = 0;

/**
 * Add a new user to connnect users pool.
 * @param {string} name 
 * @param {WebSocket} webSocket 
 */
function AddUser(webSocket, id, color)
{
    //create and add new user to pool
    var newUser = new User(webSocket, id, color);

    //add the user to its unique slot
    ConnectedUsers[newUser.ID] = newUser;

    //count users
    CountUsers();
}

/**
 * Remove a specified user from users pool.
 * @param {WebSocket} webSocket
 */
function RemoveUser(webSocket)
{
    for (var id in ConnectedUsers)
    {
        if (ConnectedUsers[id].WebSocket == webSocket)
        {
            delete ConnectedUsers[id];
        }
    }

    //count users
    CountUsers();
}

/**
 * 
 */
function CountUsers()
{
    UserCount = 0;

    for (var id in ConnectedUsers)
    {
        UserCount++;
    }

    Debug.LogStat(["Current number of connected users: ", UserCount]);
}

// Exports
module.exports.AddUser = AddUser;
module.exports.ConnectedUsers = ConnectedUsers;
module.exports.RemoveUser = RemoveUser;
module.exports.UserCount = UserCount;