//Define Base Route
let BaseRoute = require("./baseRoute");

//Define Files variables
let fileSystem = require("fs");
let JSThree;


//Read Index file.
let filePath = "./engine/website/js/three.js";
fileSystem.readFile(filePath, ReadFileCallBack);

function ReadFileCallBack(error, fileData)
{
    if (error)
    {
        console.log(error);
    }
    else
    {
        JSThree = fileData;
    }
}


/**
 * 
 */
class JsGameController extends BaseRoute
{
    /**
     * 
     * @param {*} responseResult 
     */
    Route(responseResult)
    {
        if (JSThree)
        {
            responseResult.StatusCode = 200;
            responseResult.Headers = {};
            responseResult.Headers["Content-Type"] = "text/javascript";
            responseResult.Body = JSThree;
        }
        else 
        {
            responseResult.StatusCode = 404;
            responseResult.Headers = {};
            responseResult.Headers["Content-Type"] = "text/plain";
            responseResult.Body = "What the fuck you did?";
        }
    }
}


module.exports = JsGameController;