//Defien Base Route
let BaseRoute = require("./baseRoute");

//Define Files variables
let fileSystem = require("fs");
let indexData;


//Read Index file.
let filePath = "./engine/website/index.html";
fileSystem.readFile(filePath, ReadFileCallBack);


function ReadFileCallBack(error, fileData)
{
    if (error)
    {
        console.log(error);
    }
    else
    {
        indexData = fileData;
    }
}

/**
 * 
 */
class Index extends BaseRoute
{
    /**
     * 
     * @param {ResponseResult} responseResult
     */
    Route(responseResult)
    {
        if (indexData)
        {
            responseResult.StatusCode = 200;
            responseResult.Headers = {};
            responseResult.Headers[ "Content-Type" ] = "text-html";
            responseResult.Body = indexData;
        }
        else 
        {
            responseResult.StatusCode = 404;
            responseResult.Headers = {};
            responseResult.Headers[ "Content-Type" ] = "text-html";
            responseResult.Body = "What the fuck you did?";
        }
    }
}

module.exports = Index;