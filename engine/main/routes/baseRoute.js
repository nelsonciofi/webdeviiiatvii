//define Debug
let Debug = require("../../debug/debug");

//define Router
let Router = require("../router");

/**
 * 
 */
class BaseRoute
{
    constructor(route)
    {
        this.route = route;
        Router.OnRouterRequest.on(this.route, this.Route);

        Debug.Log(["Listening on Route: ", this.route]);
    }

    /**
     * 
     * @param {ResponseResult} responseResult 
     */
    Route(responseResult)
    {
        Debug.LogError(["This is the Base Route.", responseResult]);
    }
}

module.exports = BaseRoute;
