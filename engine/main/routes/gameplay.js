//Defien Base Route
let BaseRoute = require("./baseRoute");

//Define Files variables
let fileSystem = require("fs");
let gameplayData;


//Read Index file.
let filePath = "./engine/website/gameplay.html";
fileSystem.readFile(filePath, ReadFileCallBack);


function ReadFileCallBack(error, fileData)
{
    if (error)
    {
        console.log(error);
    }
    else
    {
        gameplayData = fileData;
    }
}

/**
 * 
 */
class Gameplay extends BaseRoute
{
    /**
     * 
     * @param {ResponseResult} responseResult
     */
    Route(responseResult)
    {
        if (gameplayData)
        {
            responseResult.StatusCode = 200;
            responseResult.Headers = {};
            responseResult.Headers["Content-Type"] = "text-html";
            responseResult.Body = gameplayData;
        }
        else 
        {
            responseResult.StatusCode = 404;
            responseResult.Headers = {};
            responseResult.Headers["Content-Type"] = "text-html";
            responseResult.Body = "What the fuck you did?";
        }
    }
}

module.exports = Gameplay;