let BaseRoute = require("./baseRoute");

class Contacts extends BaseRoute
{
    /**
     * 
     * @param {ResponseResult} responseResult
     */
    Route(responseResult)
    {
        responseResult.StatusCode = 200;
        responseResult.Headers = {};
        responseResult.Headers["Content-Type"] = "text-html";

        //TODO get dynamic data from db

        responseResult.Body = "<h1>Welcome to Nelsinho's Game Server - Contact Form</h1>";
    }
}

module.exports = Contacts;
