//define Debug
const Debug = require("../debug/debug");

//define Events
const Events = require("events");


/**
 * 
 */
class Router
{
    constructor()
    {
        //Define router request event
        this.OnRouterRequest = new Events.EventEmitter();
    }

    /**
     * 
     * @param {IncomingMessage} request 
     */
    Route(request)
    {
        //logging the request - TODO > this should be an routing class that delivers generics results
        Debug.Log(["Incoming Request"]);
        Debug.Log(["Request URL: ", request.url]);
        Debug.Log(["Request Method: ", request.method]);
        for (let key in request.headers)
        {
            Debug.Log(["Request Header Entry: ", key, " : ", request.headers[key]]);
        }

        //initialize Response
        var responseResult = {};

        //Check if Url exists, otherwise, move to start
        if (this.OnRouterRequest.listeners(request.url).length <= 0)
        {
            request.url = "/";
            //TODO there must be some function to create a redirection without leave the Bad url on browser.
            //Maybe this is done in the frontEnd
        }

        //Fire Url event.
        this.OnRouterRequest.emit(request.url, responseResult);

        //return response data
        return responseResult;
    }
}


// Exports
module.exports = new Router();

//Starts all routes
StartRoutes();


/**
 * 
 */
function StartRoutes()
{
    new (require("./routes/index"))("/");
    new (require("./routes/gameplay"))("/gameplay");
    new (require("./routes/contacts"))("/contacts");
    new (require("./routes/jsGameController"))("/js/gameController.js");
    new (require("./routes/jsThree"))("/js/three.js");
}






