/**
 * Basic user object.
 */
class User
{
    constructor(webSocket, id, color)
    {
        this.Ready = false;
        this.ID = id;
        this.Color = color;
        this.WebSocket = webSocket;
    }
}

module.exports.User = User;