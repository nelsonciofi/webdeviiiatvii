//define debug
const Debug = require("../debug/debug");

//define http
const http = require("http");

//define Route
const router = require("./router");

//define httpserver
const httpServer = http.createServer((request, response) =>
{
    //handling request and result
    let requestResult = router.Route(request);

    //set response headers
    let statusCode = requestResult.StatusCode ? requestResult.StatusCode : 400;
    let headers = requestResult.Headers ? requestResult.Headers : {};
    let body = requestResult.Body ? requestResult.Body : "Cannot understand your request."

    //write response header
    response.writeHead(statusCode, headers);

    //write response content
    response.write(body);

    //close response
    response.end();
});

/**
 * Starts http server with default options.
 */
function StartDefault()
{
    //server is listening on port 3000
    Start(80);
}

/**
 * Starts http server with given options.
 * @param {number} port 
 */
function Start(port)
{
    httpServer.listen(port);
}


// Exports
module.exports.StartDefault = StartDefault;
module.exports.Start = Start;