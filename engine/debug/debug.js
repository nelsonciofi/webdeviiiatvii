var colors = require("colors");

/**
 * Add a simple log entry to console.
 * @param {string | string[]} logArgs Can be a single string or an array of strings that will be concatenated.
 */
function Log(logArgs)
{
    var logtext = "";

    if (Array.isArray(logArgs))    
    {
        logArgs.forEach(arg =>
        {
            logtext += arg;
        });
    }
    else
    {
        logtext = logArgs;
    }

    console.log(logtext.white);
}

/**
 * Add a yellow log entry in to console.
 * @param {string | string[]} logArgs Can be a single string or an array of strings that will be concatenated.
 */
function LogWarn(logArgs)
{
    var logtext = "";

    if (Array.isArray(logArgs))    
    {
        logArgs.forEach(arg =>
        {
            logtext += arg;
        });
    }
    else
    {
        logtext = logArgs;
    }

    console.log(logtext.yellow);
}

/**
 * Add a red log entry in to console.
 * @param {string | string[]} logArgs Can be a single string or an array of strings that will be concatenated.
 */
function LogError(logArgs)
{
    var logtext = "";

    if (Array.isArray(logArgs))    
    {
        logArgs.forEach(arg =>
        {
            logtext += arg;
        });
    }
    else
    {
        logtext = logArgs;
    }

    console.log(logtext.red);
}

/**
 * Add a green log entry in to console.
 * @param {string | string[]} logArgs Can be a single string or an array of strings that will be concatenated.
 */
function LogAction(logArgs)
{
    var logtext = "";

    if (Array.isArray(logArgs))    
    {
        logArgs.forEach(arg =>
        {
            logtext += arg;
        });
    }
    else
    {
        logtext = logArgs;
    }

    console.log(logtext.green);
}

/**
 * Add a blue log entry in to console.
 * @param {string | string[]} logArgs Can be a single string or an array of strings that will be concatenated.
 */
function LogStat(logArgs)
{
    var logtext = "";

    if (Array.isArray(logArgs))    
    {
        logArgs.forEach(arg =>
        {
            logtext += arg;
        });
    }
    else
    {
        logtext = logArgs;
    }

    console.log(logtext.blue);
}

module.exports.Log = Log;
module.exports.LogWarn = LogWarn;
module.exports.LogError = LogError;
module.exports.LogAction = LogAction;
module.exports.LogStat = LogStat;