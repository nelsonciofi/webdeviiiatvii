//Import Math module
var math = require("mathjs");


/**
 * Vector 3 class.
 */
class vector3 {
    constructor(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}


/**
 * Scale multiplies the components of a vector by another one and returns the resulting scaled vector.
 * @param {vector3} v1 
 * @param {vector3} v2 
 */
function Scale(v1, v2) {
    let vResult = {};

    vResult.x = v1.x * v2.x;
    vResult.y = v1.y * v2.y;
    vResult.z = v1.z * v2.z;
    return vResult;
}

/**
 * Multiplies each component of a vector by a number.
 * @param {vector3} vector 
 * @param {number} num 
 */
function Multiply(vector, num) {
    vector.x *= num;
    vector.y *= num;
    vector.z *= num;

    return vector;
}


/**
 * Divide each component of a vector by a number.
 * @param {vector3} vector 
 * @param {number} num 
 */
function Divide(vector, num) {
    vector.x /= num;
    vector.y /= num;
    vector.z /= num;

    return vector;
}


/**
 * Calculates the cross product of two vectors.
 * @param {vector3} lhs 
 * @param {vector3} rhs 
 */
function Cross(lhs, rhs) {
    let crossResult = {};
    crossResult.x = (lhs.y * rhs.z - lhs.z * rhs.y);
    crossResult.y = (lhs.z * rhs.x - lhs.x * rhs.z);
    crossResult.z = (lhs.x * rhs.y - lhs.y * rhs.x);
    return crossResult;
}

/**
 * Calculates the dot product of two vectors.
 * @param {vector3} lhs 
 * @param {vector3} rhs 
 */
function Dot(lhs, rhs) {
    return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}

/**
 * Calculates the Square Magnitude of a vector.
 */
function SqrMagnitude(vector) {
    return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
}

/**
 * Calculates the Magnitude of a vector.
 * @param {vector3} vector 
 */
function Magnitude(vector) {
    return Math.sqrt(SqrMagnitude(vector));
}

/**
 * Makes this vector have a magnitude of 1.
 * @param {vector3} vector 
 */
function Normalize(vector) {
    var magnitude = Magnitude(vector);
    if (magnitude > 9.99999974737875E-06) {
        return Divide(vector, magnitude);
    } else {
        return Properties.Zero();
    }
}

/**
 * Static properties with preset vectors.
 */
Properties =
    {
        /**
         * Shorthand for writing vector3 {0, 0, 0}
         */
        get Zero() {
            return { x: 0, y: 0, z: 0 };
        },

        /**
         * Shorthand for writing vector3 {0, 0, 1}
         */
        get Forward() {
            return { x: 0, y: 0, z: 1 };
        },

        /**
         * Shorthand for writing vector3 {0, 1, 0}
         */
        get Up() {
            return { x: 0, y: 1, z: 0 };
        },

        /**
         * Shorthand for writing vector3 {1, 0, 0}
         */
        get Right() {
            return { x: 1, y: 0, z: 0 };
        },

        /**
         * Returns a Vector3 with random values (from 0 to 1) in all its components.
         */
        get Random() {
            var randomVector = new vector3();

            randomVector.x = math.random();
            randomVector.y = math.random();
            randomVector.z = math.random();

            return randomVector;
        }
    }


/**
 * Calculates the angle between two vectors.
 * @param {vector3} vectorFrom 
 * @param {vector3} vectorTo
 */
function Angle(vectorFrom, vectorTo) {
    //find the normalized vectors and calculates the DoT
    var vFromNorm = Normalize(vectorFrom);
    var vToNorm = Normalize(vectorTo);
    var vDot = Dot(vFromNorm, vToNorm);

    //Clamp the dot result between -1 and 1
    var vDotClamped = math.max(-1, math.min(vDot, 1));

    //calculates the angle. 
    //Acos func returns a value in Radians, so we multiply by (180/pi) to convert to degrees
    var angle = math.acos(vDotClamped) * 180 / math.pi;

    //return the angle
    return angle;

    //TODO 180/Pi could be a static const var in mathjs, because it IS a constant in mathematics.
    //1rad x 180/Pi = 57,2958°
}



module.exports.Magnitude = Magnitude;
module.exports.SqrMagnitude = SqrMagnitude;
module.exports.Dot = Dot;
module.exports.Cross = Cross;
module.exports.Scale = Scale;
module.exports.Properties = Properties;
module.exports.Normalize = Normalize;
module.exports.Multiply = Multiply;
module.exports.Divide = Divide;
module.exports.Angle = Angle;