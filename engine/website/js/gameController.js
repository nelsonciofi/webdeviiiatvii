

console.log("Game Starts");


//Create Scene
var scene = new THREE.Scene();

//Create Camera
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

//Create Renderer
var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);

//Add renderer do Html
document.body.appendChild(renderer.domElement);

//Setup Input
/*
document.addEventListener("keypress", (event) =>
{
    UpdateInput(event, "press");
});
*/

document.addEventListener("keyup", (event) =>
{
    UpdateInput(event, "up");
});

document.addEventListener("keydown", (event) =>
{
    UpdateInput(event, "down");
});


var Input = {
    horizontalAxis: 0,
    verticalAxis: 0
};

/*
document.addEventListener('keypress', (event) =>
{
    const keyName = event.keyCode;
    alert('keypress event \ n \ n' + 'chave:' + keyName);
});
*/

var randomColor = Math.floor(Math.random() * 16581375);

var localPlayerGeometry = new THREE.BoxGeometry(1, 1, 1);
var localPlayerMaterial = new THREE.MeshBasicMaterial({ color: randomColor });
var localPlayerMesh = new THREE.Mesh(localPlayerGeometry, localPlayerMaterial);

//Generate Player
var LocalPlayer = {
    id: 0,
    color: randomColor,
    position: { x: 0, y: 0, z: 0 }
};

scene.add(localPlayerMesh);

//Config WebSocket
var socks = new WebSocket("ws://localhost:1337");
//var socks = new WebSocket("ws://localhost:1337");

//Local socket just opened
socks.onopen = function (e)
{
    console.log("Connection Opened: ", e);
    alert("Connection established");
}

//Local sockets get an error
socks.onerror = function (error)
{
    alert(`[error] ${error.message}`);
};

//local socket is closed
socks.onclose = function (close)
{
    console.log("Connection closed: ", close);
    alert("Connection closed");
}

//Local socket receives a message
socks.onmessage = function (e)
{
    //Parse message
    let messageData = JSON.parse(e.data);

    //log to browser console
    console.log("Message Received", messageData);

    //Update other players
    if (messageData.type == undefined)
    {
        otherPlayers[messageData.id].mesh.position.x = messageData.position.x;
        otherPlayers[messageData.id].mesh.position.y = messageData.position.y;
        otherPlayers[messageData.id].mesh.position.z = messageData.position.z;

        //stop function
        return;
    }

    //Init the Player
    if (messageData.type == "initPlayer")
    {
        //if Local player, start send connection
        if (messageData.ID == LocalPlayer.id)
        {
            UpdateConnection();
        } else
        {
            AddOtherPlayer(messageData);
        }

        //Stop function
        return;
    }

    //This is the setup message, localPlayer gets an ID from server
    if (LocalPlayer.id == 0 && messageData.type == "config")
    {
        if (messageData.type == "config")
        {
            LocalPlayer.id = messageData.ID;

            SetObstacles(messageData.Obstacles);

            var initPlayer = {
                type: "initPlayer",
                localPlayer: LocalPlayer
            };

            socks.send(JSON.stringify(initPlayer));
        }
        return;
    }

    //an user has been disconnected
    if (messageData.type == "disconnected")
    {
        RemovePlayer(messageData.id);
    }
}

/**
 * 
 * @param {int} timeInterval 
 */
function UpdateConnection(timeInterval)
{
    if (!timeInterval || timeInterval < 100)
        timeInterval = 100;

    //starts Send data update
    setInterval(() =>
    {
        if (socks.bufferedAmount == 0)
        {
            socks.send(JSON.stringify(LocalPlayer));
        }
    }, timeInterval);
}

var otherPlayers = {};

var Obstacles = [];
var obstacleSharedMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });

//Start is called once in the beginning
requestAnimationFrame(Start);

//Update is the game loop
requestAnimationFrame(Update);

/**
 * Update
 */
function Update()
{
    ControlCube();
    UpdateCamera();

    //render results
    renderer.render(scene, camera);

    //request for new frame
    requestAnimationFrame(Update);
}

function Start()
{ }

/**
 * Generates obstacle
 * @param {*} obstaclesData Obstacles data incoming from the server
 */
function SetObstacles(obstaclesData)
{
    obstaclesData.forEach(obstacleData =>
    {
        let obstacle = {};

        //base settings
        obstacle.geometry = new THREE.BoxGeometry(1, 1, 1);
        obstacle.material = obstacleSharedMaterial;
        obstacle.mesh = new THREE.Mesh(obstacle.geometry, obstacle.material);

        //set transform
        obstacle.mesh.position.x = obstacleData.position.x;
        obstacle.mesh.position.y = obstacleData.position.y;
        obstacle.mesh.position.z = obstacleData.position.z;
        obstacle.mesh.scale.x = obstacleData.scale.x;
        obstacle.mesh.scale.y = obstacleData.scale.y;
        obstacle.mesh.scale.z = obstacleData.scale.z;

        //add to array and scene
        Obstacles.push(obstacle);
        scene.add(obstacle.mesh);

    });
}

function ControlCube()
{
    localPlayerMesh.position.x += Input.horizontalAxis * 0.05;
    localPlayerMesh.position.z += Input.verticalAxis * 0.05;

    LocalPlayer.position = localPlayerMesh.position;
}

function UpdateCamera()
{
    //Camera follows player
    camera.position.x = localPlayerMesh.position.x;
    camera.position.z = localPlayerMesh.position.z + 5;
    camera.position.y = localPlayerMesh.position.y + 2;
}

/**
 * 
 * @param {*} input 
 */
function UpdateInput(event, type)
{
    //console.log(event);

    switch (type)
    {
        case "up":
            UpKey(event.keyCode);
            break;
        case "down":
            DownKey(event.keyCode);
            break;
        default:
            break;
    }
}

/*
function PressKey(keyCode)
{
    console.log("Press", keyCode);

    switch (keyCode)
    {
        case 119:
            Input.verticalAxis = 1;
            break;
        case 115:
            Input.verticalAxis = -1;
            break;
        default:
            break;
    }
}
*/

function UpKey(keyCode)
{
    switch (keyCode)
    {
        case 87:
        case 38:
        case 83:
        case 40:
            Input.verticalAxis = 0;
            break;

        case 65:
        case 37:
        case 68:
        case 39:
            Input.horizontalAxis = 0;
            break;

        default:
            break;
    }
}

function DownKey(keyCode)
{
    switch (keyCode)
    {
        case 87:
        case 38:
            Input.verticalAxis = -1;
            break;
        case 83:
        case 40:
            Input.verticalAxis = 1;
            break;
        case 65:
        case 37:
            Input.horizontalAxis = -1;
            break;
        case 68:
        case 39:
            Input.horizontalAxis = 1;
        default:
            break;
    }
}

/**
 * 
 * @param {*} playerID 
 */
function RemovePlayer(playerID)
{
    console.log("Leaving Player: ", playerID);
    otherPlayers[playerID].mesh.material.dispose();
    otherPlayers[playerID].mesh.geometry.dispose();
    scene.remove(otherPlayers[playerID].mesh);

    delete otherPlayers.playerID;
}

/**
 * 
 * @param {*} player 
 */
function AddOtherPlayer(player)
{
    console.log("Incoming Player: ", player);

    let otherPlayerGeometry = new THREE.BoxGeometry(1, 1, 1);
    let otherPlayerMaterial = new THREE.MeshBasicMaterial({ color: player.Color });
    let otherPlayerMesh = new THREE.Mesh(otherPlayerGeometry, otherPlayerMaterial);

    let someOtherPlayer = {};
    someOtherPlayer.mesh = otherPlayerMesh;
    scene.add(someOtherPlayer.mesh);
    otherPlayers[player.ID] = someOtherPlayer;
}