//set Debug
const Debug = require("./engine/debug/debug");

Debug.Log("Server is about to start.");

// Starts Http Server
require("./engine/main/httpServer").StartDefault();

// Starts WebSocket Server
require("./engine/main/socketServer").Start();

Debug.Log("Server is running. Send me your requests babe!");